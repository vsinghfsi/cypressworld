describe('Test 1', () => {
  it('Visits a web page', () => {
   //cy.visit(Cypress.config().baseUrl)
   cy.visit("www.amazon.com")
  })
})

describe('Test 2', () => {
  it('Visits a web page via baseUrl env variable', () => {
   cy.visit(Cypress.config().baseUrl)
  })
})

describe('Test 3', () => {
  it('Searches via feeling lucky google feature', () => {
    cy.get('#gbqfbb').trigger('mouseover')
    cy.wait(1000)
    // cy.get('#gbqfbb').click()
  })
})

describe('Test 4', () => {
  it('Performs a google search', () => {
    cy.visit(Cypress.config().baseUrl)
    cy.get('.gLFyf').type("potato")
    cy.get('.aajZCb > .tfB0Bf > center > .gNO89b').click()
    //cy.get('#gbqfbb').click()
  })
})

describe('Test 5', () => {
  it('Scroll through search results', () => {
    cy.scrollTo('bottom')
    cy.scrollTo('top')
  })
})

describe('Test 6', () => {
  it('Select search result', () => {
    cy.wait(1000)
    cy.get('.ifM9O > :nth-child(2) > .g').click()
  })
})

describe('Test 7', () => {
  it('Random assertion test', () => {
    cy.expect(10).to.equal(10)
  })
})